export function notFoundImage(width = 100, height = 100) {
  return `https://dummyimage.com/${width}x${height}/000/ffffff.png&text=Not+Found+:(`;
}

export function isImageExist(imageUrl) {
  return fetch(imageUrl)
    .then((res) => res)
    .catch((err) => err);
}

export function getDataUrl(img) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");

  canvas.width = img.width;
  canvas.height = img.height;
  ctx.drawImage(img, 0, 0);

  // If the image is not png, the format
  // must be specified here
  return canvas.toDataURL();
}
