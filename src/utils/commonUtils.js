const isEmpty = (object) => {
  return (
    object === undefined ||
    object.length === 0 ||
    Object.entries(object).length === 0
  );
};

function checkNextPage(total, page, limit) {
  if (total > limit * page) return true;
  return false;
}

function timeoutFunc(timeout, action, delay) {
  clearTimeout(timeout);
  return setTimeout(function () {
    action();
  }, delay);
}

function copyToClipBoard(textToCopy) {
  const el = document.createElement('textarea');
  el.value = textToCopy;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}

export { isEmpty, checkNextPage, timeoutFunc, copyToClipBoard };
