export function getStoreId(fullUrl = '') {
  let currentLocation;
  if (fullUrl !== '' || fullUrl !== undefined) {
    currentLocation = fullUrl;
  }
  if (process.browser) {
    currentLocation = window.location.href;
  }
  return currentLocation.split('.')?.[0]?.replace(/http:\/\/|https:\/\//g, '');
}
