import React from "react";

const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));
const Attendance = React.lazy(() => import("./views/attendance"));
const Thermal = React.lazy(() => import("./views/thermal"));
const FaceRecognation = React.lazy(() => import("./views/faceRecognation"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/attendance", name: "Presensi", component: Attendance },
  { path: "/thermal-imaging", name: "Thermal Imagning", component: Thermal },
  {
    path: "/face-recognation",
    name: "Face Recognation",
    component: FaceRecognation,
  },
];

export default routes;
