import React from "react";
import { Form, Button, Input, message } from "antd";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import IMAGES from "../../../assets";
import { setLocalStorage } from "../../../utils/localStorage";

const Login = () => {
  const onFinish = (body) => {
    const { username, password } = body;
    const authentication = JSON.parse(process.env.REACT_APP_REST_AUTH);
    if (
      authentication.username === username &&
      authentication.password === password
    ) {
      setLocalStorage(
        "is_authenticated",
        process.env.REACT_APP_REST_IS_AUTHENTICATED
      );
      setTimeout(() => {
        window.location.replace("/");
        // setIsShow(false);
      }, 3000);
    } else {
      message.error("username / password salah");
    }
  };

  return (
    <>
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Masuk ke dalam dashboard</p>
                    <Form name="basic" onFinish={onFinish}>
                      <Form.Item
                        name="username"
                        rules={[
                          {
                            required: true,
                            message: "Silahkan masukkan username!",
                          },
                        ]}
                        hasFeedback
                      >
                        <Input prefix={<CIcon name="cil-user" />} />
                      </Form.Item>

                      <Form.Item
                        name="password"
                        rules={[
                          {
                            required: true,
                            message: "Silahkan masukkan password!",
                          },
                        ]}
                        hasFeedback
                      >
                        <Input.Password
                          prefix={<CIcon name="cil-lock-locked" />}
                        />
                      </Form.Item>

                      <Form.Item>
                        <Button type="primary" htmlType="submit" block>
                          Submit
                        </Button>
                      </Form.Item>
                    </Form>
                  </CCardBody>
                </CCard>
                <CCard
                  className=" py-5 d-md-down-none"
                  style={{ width: "44%" }}
                >
                  <CCardBody className="text-center">
                    <div className="component_login_image_wrapper">
                      <img alt="logo" src={IMAGES.LOGO} />
                      <img alt="logo" src={IMAGES.KEMENTRIAN} />
                    </div>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    </>
  );
};

export default Login;
