import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const AttendancePerDay = lazy(() =>
  import("../../containers/attendancePerDay")
);
const AttendancePerMonth = lazy(() =>
  import("../../containers/attendancePerMonth")
);
const AttendancePerDivision = lazy(() =>
  import("../../containers/attendancePerDivision")
);

const Dashboard = () => {
  return (
    <CRow>
      <CCol>
        <AttendancePerDay />
        <AttendancePerMonth />
        <AttendancePerDivision />
      </CCol>
    </CRow>
  );
};

export default Dashboard;
