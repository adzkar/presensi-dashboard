import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const BestAttendancePerMonth = lazy(() =>
  import("../../containers/bestAttendancePerMonth")
);
const MostAbsencePerMonth = lazy(() =>
  import("../../containers/mostAbsencePerMonth")
);
const MostLatenessPerMonth = lazy(() =>
  import("../../containers/mostLatenessPerMonth")
);
const Pie = lazy(() => import("../../containers/dashboardPie"));

const Dashboard = () => {
  return (
    <>
      <CRow>
        <CCol>
          <BestAttendancePerMonth />
          <MostAbsencePerMonth />
          <MostLatenessPerMonth />
        </CCol>
      </CRow>
      <CRow>
        <CCol>
          <Pie />
        </CCol>
        <CCol></CCol>
      </CRow>
    </>
  );
};

export default Dashboard;
