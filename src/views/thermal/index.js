import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const Thermal = lazy(() => import("../../containers/thermal"));

const Dashboard = () => {
  return (
    <CRow>
      <CCol>
        <Thermal />
      </CCol>
    </CRow>
  );
};

export default Dashboard;
