import BaseService from "../BaseService";
import URL from "../../config/baseUrl";

function getDivision() {
  return BaseService.get(URL.DIVISION_TYPE);
}

export default {
  getDivision,
};
