import BaseService from "../BaseService";
import URL from "../../config/baseUrl";

function getBest(periode, rest) {
  const params = {
    limit: 5,
    periode: periode,
    ...rest,
  };
  return BaseService.get(URL.USER_BEST, { params: params });
}

function getAbsence(periode, rest) {
  const params = {
    limit: 5,
    periode: periode,
    ...rest,
  };
  return BaseService.get(URL.USER_ABSENCE, { params: params });
}

function getLateness(periode, rest) {
  const params = {
    limit: 5,
    periode: periode,
    ...rest,
  };
  return BaseService.get(URL.USER_LATENESS, { params: params });
}

function getPie() {
  return BaseService.get(URL.DASHBOARD_PIE);
}

export default {
  getBest,
  getAbsence,
  getLateness,
  getPie,
};
