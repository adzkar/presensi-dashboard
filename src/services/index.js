export { default as AttendanceService } from "./attendance";
export { default as DivisionService } from "./division";
export { default as DashboardService } from "./dashboard";
export { default as FaceRecognationService } from "./faceRecognation";
