import { DashboardService } from "../../services";
import useSWR from "swr";
import URL from "../../config/baseUrl";

const useGetBest = (periode, params) => {
  return useSWR(URL.USER_BEST, (URL) =>
    DashboardService.getBest(periode, params).then((res) => res)
  );
};

const useGetAbsence = (periode, params) => {
  return useSWR(URL.USER_ABSENCE, (URL) =>
    DashboardService.getAbsence(periode, params).then((res) => res)
  );
};

const useGetLatness = (periode, params) => {
  return useSWR(URL.USER_LATENESS, (URL) =>
    DashboardService.getLatness(periode, params).then((res) => res)
  );
};

const useGetPie = (periode, params) => {
  return useSWR(URL.DASHBOARD_PIE, (URL) =>
    DashboardService.getPie().then((res) => res)
  );
};

export default { useGetBest, useGetAbsence, useGetLatness, useGetPie };
