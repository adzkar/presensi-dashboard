import { FaceRecognationService } from "../../services";
import useSWR from "swr";
import URL from "../../config/baseUrl";

const useGetDatas = () => {
  return useSWR(URL.DIVISION_TYPE, (URL) =>
    FaceRecognationService.getDatas().then((res) => res)
  );
};

export default { useGetDatas };
