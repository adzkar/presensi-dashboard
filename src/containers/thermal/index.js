import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardTitle,
  CCardText,
} from "@coreui/react";
import { Row, Col, Button } from "antd";
import { FileImageOutlined } from "@ant-design/icons";
import "./thermal.style.scss";

const Index = () => {
  const [captured, setCaptured] = useState();

  const URL = "http://192.168.20.10:8000";

  const onCapture = () => {
    let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=400,height=500,left=-1000,top=-1000`;
    var wnd = window.open(URL, "thermal", params);
    setTimeout(function () {
      wnd.close();
      setCaptured(`${URL}/static/savedImage.jpg`);
    }, 3000);
    return false;
  };

  return (
    <CCard className="container_thermal_content_wrapper" id="contoh">
      <CCardHeader>Thermal Imaging</CCardHeader>
      <CCardBody>
        <Row className="container_thermal_wrapper">
          <Col flex="2">
            <CCard>
              <CCardBody>
                <CCardTitle>Streaming Video Thermal</CCardTitle>
                <CCardText>
                  <span id="streaming">
                    <img alt="gambar" src={`${URL}/video_feed`} id="gambar" />
                  </span>
                </CCardText>
              </CCardBody>
            </CCard>
          </Col>
          <Col flex="2">
            <CCard>
              <CCardBody>
                <CCardTitle>Gambar Last Capture</CCardTitle>
                <CCardText>
                  <span className="image_wrapper">
                    {captured && <img alt="captured" src={captured} />}
                  </span>
                </CCardText>
              </CCardBody>
            </CCard>
          </Col>
        </Row>
        <Row className="container_thermal_button_wrapper">
          <Col flex>
            <Button type="primary" onClick={onCapture}>
              <FileImageOutlined />
              <span className="text-center">Refresh</span>
            </Button>
          </Col>
        </Row>
      </CCardBody>
    </CCard>
  );
};

export default Index;
