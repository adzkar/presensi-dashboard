import React, { useState, useLayoutEffect } from "react";
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index";
import IMAGES from "../assets";

const TheLayout = () => {
  const [isShow, setIsShow] = useState(true);
  useLayoutEffect(() => {
    setTimeout(() => {
      setIsShow(false);
    }, 3000);
  }, []);

  return (
    <>
      <div
        style={{
          display: isShow ? "block" : "none",
          transition: "all 0.3s ease-in",
        }}
      >
        <div className="component_splash_screen ">
          <p>
            Selamat Datang di Aplikasi Presensi Social Distancinng Covid-19
            <br />
            Kerjasama Riset Bandung Techno Park Telkom University dan Kementrian
            Perindustrian RI
            <br />
          </p>
          <div>
            <img alt="logo" src={IMAGES.LOGO} />
            <img alt="logo" src={IMAGES.KEMENTRIAN} />
          </div>
        </div>
      </div>
      <div className="c-app c-default-layout">
        <TheSidebar />
        <div className="c-wrapper">
          <TheHeader />
          <div className="c-body">
            <TheContent />
          </div>
          <TheFooter />
        </div>
      </div>
    </>
  );
};

export default TheLayout;
