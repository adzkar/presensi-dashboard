import React from "react";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import { CChartPie } from "@coreui/react-chartjs";
import { DashboardCustomHook } from "../../customHooks";

const Index = () => {
  const { data } = DashboardCustomHook.useGetPie();

  return (
    <CCard>
      <CCardHeader>Persentase Kehadira Hari Ini</CCardHeader>
      <CCardBody>
        {data !== undefined && (
          <CChartPie
            type="pie"
            datasets={[
              {
                backgroundColor: ["#41B883", "#fedd53", "#00D8FF", "#DD1B16"],
                data: Object.values(data?.[0]),
              },
            ]}
            labels={["Tepat Waktu", "Terlambat", "Izin", "Tidak Hadir"]}
            options={{
              tooltips: {
                enabled: true,
              },
            }}
          />
        )}
      </CCardBody>
    </CCard>
  );
};

export default Index;
