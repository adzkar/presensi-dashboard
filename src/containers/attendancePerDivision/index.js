import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CCardTitle,
} from "@coreui/react";
import { AttendanceCustomHook, DivisionCustomHook } from "../../customHooks";
import { AttendanceService } from "../../services";
import { DatePicker, Select } from "antd";
import { TimeFormatter } from "../../utils/timeParser";
import Moment from "moment";

const Index = () => {
  const [currentDate, setCurrentDate] = useState(Moment().format("YYYY-MM-DD"));
  const [currentDivision, setCurrentDivison] = useState(1);
  const { data, mutate } = AttendanceCustomHook.useGetAttendance(
    "",
    "",
    {
      isGetAllDate: true,
      isByDivisionTypeOnly: true,
      division_type_id: currentDivision,
    },
    "division"
  );
  const { data: divisionData } = DivisionCustomHook.useGetDivision();

  useEffect(() => {
    if (divisionData !== undefined) {
      const first = divisionData[0]?.id;
      setCurrentDivison(first);
      mutate((current) => {
        return AttendanceService.getAttendance("", "", {
          isGetAllDate: true,
          isByDivisionTypeOnly: true,
          division_type_id: first,
        }).then((res) => res);
      }, false);
    }
  }, [divisionData, mutate]);

  const onChangeDatePicker = (e) => {
    if (e) {
      const d = e.format("YYYY-MM-DD");
      setCurrentDate(d);
      mutate((current) => {
        return AttendanceService.getAttendance("", "", {
          tanggal: d,
        }).then((res) => res);
      }, false);
    } else {
      setCurrentDate("Semua Hari");
      mutate((current) => {
        return AttendanceService.getAttendance("", "", {
          isGetAllDate: true,
        }).then((res) => res);
      }, false);
    }
  };

  const onChangeDivison = (value) => {
    mutate((current) => {
      return AttendanceService.getAttendance("", "", {
        isGetAllDate: true,
        isByDivisionTypeOnly: true,
        division_type_id: value,
      }).then((res) => {
        setCurrentDivison(value);
        return res;
      });
    }, false);
  };

  const fields = [
    { key: "no", label: "No" },
    { key: "user_noid", label: "NIP" },
    { key: "user_name", label: "Name" },
    { key: "user_sex", label: "Jenis Kelamin" },
    { key: "division_name", label: "Divisi" },
    { key: "waktu_masuk", label: "Waktu Masuk" },
    { key: "isHadir", label: "Kehadiran" },
    { key: "keterangan", label: "Keterangan" },
    { key: "tanggal", label: "Tanggal" },
  ];

  return (
    <CCard>
      <CCardHeader>
        <div className="container_attendance_per_day_header">
          <CCardTitle>Kehadiran PerDivisi</CCardTitle>

          <DatePicker onChange={onChangeDatePicker} defaultValue={Moment()} />
        </div>
      </CCardHeader>
      <CCardBody>
        <p>Menampilkan data pada tanggal: {currentDate}</p>
        <div className="card-header-actions">
          Divisi:{" "}
          <Select
            style={{ width: "120px" }}
            value={currentDivision}
            onChange={onChangeDivison}
          >
            {divisionData !== undefined &&
              divisionData.map((item, i) => {
                return (
                  <Select.Option key={i} value={item.id}>
                    {item.name}
                  </Select.Option>
                );
              })}
          </Select>
        </div>
        <CDataTable
          items={data}
          fields={fields}
          pagination
          hover
          sorter
          tableFilter
          itemsPerPageSelect
          itemsPerPage={5}
          loading={data === undefined}
          scopedSlots={{
            no: (item, index) => {
              return <td>{index + 1}</td>;
            },
            user_sex: (item) => {
              return (
                <td>{item.user_sex === "M" ? "Laki-Laki" : "Perempuan"}</td>
              );
            },
            isHadir: (item) => {
              return <td>{item.isHadir ? "Hadir" : "Tidak Hadir"}</td>;
            },
            description: (item) => {
              return (
                <td>{item.description === null ? "-" : item.description}</td>
              );
            },
            tanggal: (item) => {
              const parsed = TimeFormatter(item.tanggal);
              return (
                <td>{`${parsed.date} ${parsed.monthName} ${parsed.year}`}</td>
              );
            },
            waktu_masuk: (item) => {
              if (item !== null) {
                const parsed = TimeFormatter(item.waktu_masuk);
                return (
                  <td className="text-center">{`${parsed.hour}:${parsed.min}`}</td>
                );
              } else {
                return <td className="text-center">-</td>;
              }
            },
            // periode: (item) => {
            //   if (item?.periode) {
            //     return (
            //       <td>
            //         {months[Number(item?.periode?.slice(-1)) - 1]}{" "}
            //         {item?.periode?.slice(0, -1)}
            //       </td>
            //     );
            //   } else {
            //     return <td></td>;
            //   }
            // },
          }}
        />
      </CCardBody>
    </CCard>
  );
};

export default Index;
