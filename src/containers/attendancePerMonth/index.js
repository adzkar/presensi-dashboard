import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CCardTitle,
} from "@coreui/react";
import { AttendanceCustomHook } from "../../customHooks";
import { AttendanceService } from "../../services";
import { DatePicker } from "antd";
import { TimeFormatter } from "../../utils/timeParser";
import Moment from "moment";
import { months } from "../../config/months";

const Index = () => {
  const [currentDate, setCurrentDate] = useState(Moment().format("YYYYM"));
  const { data, mutate } = AttendanceCustomHook.useGetAttendance(
    "",
    "",
    {
      periode: Moment().format("YYYYM"),
    },
    "month"
  );

  const onChangeDatePicker = (e) => {
    if (e) {
      const d = e.format("YYYYM");
      setCurrentDate(d);
      mutate((current) => {
        return AttendanceService.getAttendance("", "", {
          periode: d,
        }).then((res) => res);
      }, false);
    } else {
      setCurrentDate("Semua Bulan ");
      mutate((current) => {
        return AttendanceService.getAttendance("", "", {
          isGetAllDate: true,
        }).then((res) => res);
      }, false);
    }
  };

  const fields = [
    { key: "no", label: "No" },
    { key: "user_noid", label: "NIP" },
    { key: "user_name", label: "Name" },
    { key: "user_sex", label: "Jenis Kelamin" },
    { key: "division_name", label: "Divisi" },
    { key: "waktu_masuk", label: "Waktu Masuk" },
    { key: "isHadir", label: "Kehadiran" },
    { key: "keterangan", label: "Keterangan" },
    { key: "tanggal", label: "Tanggal" },
  ];

  return (
    <CCard>
      <CCardHeader>
        <div className="container_attendance_per_day_header">
          <CCardTitle>Kehadiran Perbulan</CCardTitle>
          <DatePicker
            onChange={onChangeDatePicker}
            defaultValue={Moment()}
            picker="month"
          />
        </div>
      </CCardHeader>
      <CCardBody>
        <p>
          Menampilkan data pada bulan:{" "}
          {months[Number(currentDate.slice(-1)) - 1]} {currentDate.slice(0, -1)}
        </p>
        <CDataTable
          items={data}
          fields={fields}
          pagination
          hover
          sorter
          tableFilter
          itemsPerPageSelect
          itemsPerPage={5}
          loading={data === undefined}
          scopedSlots={{
            no: (item, index) => {
              return <td>{index + 1}</td>;
            },
            user_sex: (item) => {
              return (
                <td>{item.user_sex === "M" ? "Laki-Laki" : "Perempuan"}</td>
              );
            },
            isHadir: (item) => {
              return <td>{item.isHadir ? "Hadir" : "Tidak Hadir"}</td>;
            },
            description: (item) => {
              return (
                <td>{item.description === null ? "-" : item.description}</td>
              );
            },
            tanggal: (item) => {
              const parsed = TimeFormatter(item.tanggal);
              return (
                <td>{`${parsed.date} ${parsed.monthName} ${parsed.year}`}</td>
              );
            },
            waktu_masuk: (item) => {
              if (item !== null) {
                const parsed = TimeFormatter(item.waktu_masuk);
                return (
                  <td className="text-center">{`${parsed.hour}:${parsed.min}`}</td>
                );
              } else {
                return <td className="text-center">-</td>;
              }
            },
            // periode: (item) => {
            //   if (item?.periode) {
            //     return (
            //       <td>
            //         {months[Number(item?.periode?.slice(-1)) - 1]}{" "}
            //         {item?.periode?.slice(0, -1)}
            //       </td>
            //     );
            //   } else {
            //     return <td></td>;
            //   }
            // },
          }}
        />
      </CCardBody>
    </CCard>
  );
};

export default Index;
