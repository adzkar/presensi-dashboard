export default [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: "cil-speedometer",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Presensi",
    to: "/attendance",
    icon: "cil-bookmark",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Face Recognation",
    to: "/face-recognation",
    icon: "cil-bookmark",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Thermal Imaging",
    to: "/thermal-imaging",
    icon: "cil-speedometer",
  },
];
