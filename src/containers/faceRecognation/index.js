import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalHeader,
  CModalBody,
  CRow,
  CCol,
} from "@coreui/react";
// import { faces as data } from "./face.dummy";
import { FaceRecognationCustomHook } from "../../customHooks";
import "./face.style.scss";

const Index = () => {

  const [isShowModal, setIsShowModal] = useState(false);
  const [selectedData, setSelectedData] = useState("");

  const { data } = FaceRecognationCustomHook.useGetDatas()

  const fields = [
    { key: "id", label: "No" },
    { key: "nip", label: "NIP" },
    { key: "nama", label: "Nama" },
    { key: "tanggal", label: "Tanggal" },
    {
      key: "foto",
      label: "foto",
      _style: { display: "none" },
      sorter: false
    },
    { key: "detail", label: "Detail", _classes: "text-center", sorter: false },
  ];

  return (
    <>
      <CModal
        show={isShowModal}
        onClose={() => {
          setSelectedData("");
          setIsShowModal(false);
        }}
        color="success"
        variant="ghost"
      >
        <CModalHeader closeButton>Detail Information</CModalHeader>
        <CModalBody>
          {selectedData && (
            <CRow>
              <CCol md="4">
                <div className="container_face_recognation_image">
                  <img src={`data:image/jpeg;base64, ${selectedData?.foto?.trim()}`} alt="foto"/>
                </div>
              </CCol>
              <CCol span="10">
                <CCard>
                  <CCardBody>
                    <CRow>
                      <CCol span="2">
                        <strong>Tanggal</strong>
                      </CCol>
                      <CCol span="10">{selectedData?.tanggal}</CCol>
                    </CRow>
                    <CRow>
                      <CCol span="2">
                        <strong>Jam</strong>
                      </CCol>
                      <CCol span="10">{selectedData?.waktu}</CCol>
                    </CRow>
                    <CRow>
                      <CCol span="2">
                        <strong>NIP</strong>
                      </CCol>
                      <CCol span="10">{selectedData?.nip}</CCol>
                    </CRow>
                    <CRow>
                      <CCol span="2">
                        <strong>Nama</strong>
                      </CCol>
                      <CCol span="10">{selectedData?.nama}</CCol>
                    </CRow>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          )}
        </CModalBody>
      </CModal>
      <CCard>
        <CCardHeader>Face Recognation</CCardHeader>
        <CCardBody>
          <CDataTable
            items={data}
            fields={fields}
            pagination
            hover
            sorter
            tableFilter
            itemsPerPageSelect
            itemsPerPage={5}
            loading={data === undefined}
            scopedSlots={{
              id: (item, index) => {
                return <td>{index + 1}</td>;
              },
              foto: (item) => {
                return <td className="component_none"></td>;
              },
              detail: (item) => {
                return (
                  <td className="text-center">
                    <CButton
                      color="success"
                      variant="outline"
                      className="px-4"
                      onClick={() => {
                        setIsShowModal(true);
                        setSelectedData(item);
                      }}
                    >
                      Detail
                    </CButton>
                  </td>
                );
              },
            }}
          />
        </CCardBody>
      </CCard>
    </>
  );
};

export default Index;
