import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CCardTitle,
} from "@coreui/react";
import { DashboardCustomHook, DivisionCustomHook } from "../../customHooks";
import { DashboardService } from "../../services";
import { DatePicker, Select } from "antd";
import Moment from "moment";
// import CIcon from "@coreui/icons-react";

const Index = () => {
  const [currentDate, setCurrentDate] = useState(Moment().format("YYYYM"));
  const [currentDivision, setCurrentDivison] = useState(1);
  const { data, mutate } = DashboardCustomHook.useGetLatness(currentDate, {
    division_type_id: currentDivision,
  });
  const { data: divisionData } = DivisionCustomHook.useGetDivision();

  useEffect(() => {
    if (divisionData !== undefined) {
      const first = divisionData[0]?.id;
      setCurrentDivison(first);
      mutate((current) => {
        return DashboardService.getLateness(currentDate, {
          division_type_id: first,
        });
      }, false);
    }
  }, [divisionData, mutate, currentDate]);

  const onChangeDatePicker = (e) => {
    if (e) {
      const d = e.format("YYYYM");
      setCurrentDate(d);
      mutate((current) => {
        return DashboardService.getLateness(d, {
          division_type_id: 1,
        }).then((res) => res);
      }, false);
    } else {
    }
  };

  const onChangeDivison = (value) => {
    mutate((current) => {
      return DashboardService.getLateness(currentDate, {
        division_type_id: value,
      }).then((res) => {
        setCurrentDivison(value);
        return res;
      });
    }, false);
  };

  const fields = [
    { key: "no", label: "No" },
    { key: "user_noid", label: "NIP" },
    { key: "user_name", label: "Name" },
    { key: "user_sex", label: "Jenis Kelamin" },
    { key: "division_name", label: "Divisi" },
    { key: "periode", label: "Periode" },
  ];

  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Des",
  ];

  return (
    <CCard>
      <CCardHeader>
        <div className="container_attendance_per_day_header">
          <CCardTitle>Pegawai dengan keterlambatan tertinggi</CCardTitle>
          <DatePicker
            onChange={onChangeDatePicker}
            defaultValue={Moment()}
            picker="month"
          />
        </div>
      </CCardHeader>
      <CCardBody>
        <p>
          Menampilkan data pada bulan:{" "}
          {months[Number(currentDate.slice(-1)) - 1]} {currentDate.slice(0, -1)}
        </p>
        <div className="card-header-actions">
          Divisi:{" "}
          <Select
            style={{ width: "120px" }}
            value={currentDivision}
            onChange={onChangeDivison}
          >
            {divisionData !== undefined &&
              divisionData.map((item, i) => {
                return (
                  <Select.Option key={i} value={item.id}>
                    {item.name}
                  </Select.Option>
                );
              })}
          </Select>
        </div>
        <CDataTable
          items={data}
          fields={fields}
          hover
          tableFilter
          loading={data === undefined}
          scopedSlots={{
            no: (item, index) => {
              return <td>{index + 1}</td>;
            },
            user_sex: (item) => {
              return (
                <td>{item.user_sex === "M" ? "Laki-Laki" : "Perempuan"}</td>
              );
            },
            // periode: (item) => {
            //   if (item?.periode) {
            //     return (
            //       <td>
            //         {months[Number(item?.periode?.slice(-1)) - 1]}{" "}
            //         {item?.periode?.slice(0, -1)}
            //       </td>
            //     );
            //   } else {
            //     return <td></td>;
            //   }
            // },
          }}
        />
      </CCardBody>
    </CCard>
  );
};

export default Index;
